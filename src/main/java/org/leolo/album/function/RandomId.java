package org.leolo.album.function;

import java.io.IOException;
import java.util.Random;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.leolo.album.JSONResponse;
import org.leolo.album.Utils;

/**
 * Servlet implementation class RandomId
 */
@WebServlet("/RandomId")
public class RandomId extends HttpServlet {
	private static final long serialVersionUID = 1L;
	private org.slf4j.Logger logger = org.slf4j.LoggerFactory.getLogger(RandomId.class); 
	
	public final char[] CHARS = "1234567890QWERTYUIOPASDFGHJKLZXCVBNMqwertyuiopasdfghjklzxcvbnm-_".toCharArray();
	
    /**
     * @see HttpServlet#HttpServlet()
     */
    public RandomId() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		JSONResponse resp = new JSONResponse();
		if(request.getParameter("length")==null){
			resp.put("Result","Error");
			resp.put("message", "Parameter required");
			response.setStatus(HttpServletResponse.SC_BAD_REQUEST);
			response.setContentType(resp.getContentType());
			resp.write(response.getOutputStream());
			return;
		}
		int length;
		try{
			length = Integer.parseInt(request.getParameter("length"));
		}catch(NumberFormatException nfe){
			resp.put("Result","Error");
			resp.put("message", "Illegal number format");
			response.setStatus(HttpServletResponse.SC_BAD_REQUEST);
			response.setContentType(resp.getContentType());
			resp.write(response.getOutputStream());
			return;
		}
		if(length > 128 || length < 0){
			resp.put("Result","Error");
			resp.put("message", "Illegal request length");
			response.setStatus(HttpServletResponse.SC_BAD_REQUEST);
			response.setContentType(resp.getContentType());
			resp.write(response.getOutputStream());
			return;
		}
		StringBuilder sb = new StringBuilder();
		Random r = new Random(Utils.nextRandomSeed());
		for(int i=0;i<length;i++){
			sb.append(CHARS[r.nextInt(CHARS.length)]);
		}
		resp.put("Result","OK");
		resp.put("id", sb);
		response.setContentType(resp.getContentType());
		resp.write(response.getOutputStream());
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		doGet(request, response);
	}

}
